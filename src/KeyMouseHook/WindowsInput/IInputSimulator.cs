﻿using System;
using System.Collections.Generic;

namespace Loamen.KeyMouseHook
{
    /// <summary>
    /// The contract for a service that simulates Keyboard and Mouse input and Hardware Input Device state detection for the Windows Platform.
    /// </summary>
    public interface IInputSimulator
    {
        /// <summary>
        /// Get or set enable events
        /// </summary>
        Dictionary<MacroEventType, bool> EnableEventTypes { get; }
        /// <summary>
        /// Callback event
        /// </summary>
        event EventHandler<MacroEvent> OnPlayback;
        /// <summary>
        /// Gets the <see cref="IKeyboardSimulator"/> instance for simulating Keyboard input.
        /// </summary>
        /// <value>The <see cref="IKeyboardSimulator"/> instance.</value>
        IKeyboardSimulator Keyboard { get; }

        /// <summary>
        /// Gets the <see cref="IMouseSimulator"/> instance for simulating Mouse input.
        /// </summary>
        /// <value>The <see cref="IMouseSimulator"/> instance.</value>
        IMouseSimulator Mouse { get; }

        /// <summary>
        /// Gets the <see cref="IInputDeviceStateAdaptor"/> instance for determining the state of the various input devices.
        /// </summary>
        /// <value>The <see cref="IInputDeviceStateAdaptor"/> instance.</value>
        IInputDeviceStateAdaptor InputDeviceState { get; }

        IInputSimulator Enable(MacroEventType macroEventType);

        void PlayBack(IList<MacroEvent> mouseKeyEventList);
    }
}